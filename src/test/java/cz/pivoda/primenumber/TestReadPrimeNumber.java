package cz.pivoda.primenumber;

import org.apache.poi.xssf.usermodel.XSSFSheet;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;
import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;


/**
 * @author Marek Pivoda (pivodmar)
 * 2020-06-05
 */
class TestReadPrimeNumber {
    static XSSFSheet sheet;
    final static String FILE_NAME = "sample_data.xlsx";

    static ReadPrimeNumber primeReader;

    @BeforeAll
    static void init() {
        try {
            primeReader = new ReadPrimeNumber();
            primeReader.initFile(TestReadPrimeNumber.class.getClassLoader().getResource(FILE_NAME).getFile());
        } catch (IOException e) {

        }
    }

    @Test
    public void testInitFile() {
        assertNotNull(primeReader.getSheet());
    }

    @Test
    public void TestArraySizePrimeNumbers() {
        assertEquals(8, primeReader.getPrimeNumbers().size());
    }

    @Test
    void TestCheckPrimeNumber() {
        assertTrue(primeReader.isPrimeNumber(7));
    }

    @Test
    void TestCheckNoPrimeNumber() {
        assertFalse(primeReader.isPrimeNumber(8));
    }
}