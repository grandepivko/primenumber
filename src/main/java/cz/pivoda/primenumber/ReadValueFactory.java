package cz.pivoda.primenumber;

import org.apache.poi.ss.usermodel.Cell;

/**
 * @author Marek Pivoda (pivodmar)
 * 2020-06-05
 */
public class ReadValueFactory {

    public static ReadValueFromExcelFile getReader(Cell cell) {
        if (cell == null) {
            return null;
        }

        switch (cell.getCellType()) {
            case STRING:
                return new ReadStringValue();
            case NUMERIC:
                return new ReadNumberValue();
            default:
                return null;
        }
    }

}
