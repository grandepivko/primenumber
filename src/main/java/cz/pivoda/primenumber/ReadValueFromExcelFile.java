package cz.pivoda.primenumber;

import org.apache.poi.ss.usermodel.Cell;

/**
 * @author Marek Pivoda (pivodmar)
 * 2020-06-05
 */
public interface ReadValueFromExcelFile {

    int getValue(Cell cell);
}
