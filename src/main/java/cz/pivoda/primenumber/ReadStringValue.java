package cz.pivoda.primenumber;

import org.apache.poi.ss.usermodel.Cell;

/**
 * @author Marek Pivoda (pivodmar)
 * 2020-06-05
 */
public class ReadStringValue implements ReadValueFromExcelFile {
    public final static String REGEX_DOUBLE = "^[0-9]+[\\\\,\\\\.]{1}([0-9]+)?$";

    

    private int getInteger(String primeNumber) {
        if (!primeNumber.matches(REGEX_DOUBLE)) {
            try {
                return Integer.parseInt(primeNumber);
            } catch (NumberFormatException nex) {
                return 0;
            }
        }
        return 0;
    }

    private boolean isBlank(String str) {
        return str == null || str.trim().equals("");
    }

    public int getValue(Cell cell) {
        if (!isBlank(cell.getStringCellValue())) {
            return getInteger(cell.getStringCellValue());
        }
        return 0;
    }
}
