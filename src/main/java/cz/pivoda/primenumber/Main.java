package cz.pivoda.primenumber;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author Marek Pivoda (pivodmar)
 * 2020-06-05
 */
public class Main {

    public static void main(String[] args) {
        if (args.length > 0) {
            String filePath = args[0];
            try {
                ReadPrimeNumber reader = new ReadPrimeNumber();
                reader.initFile(filePath);
                for (int prime : reader.getPrimeNumbers()) {
                    System.out.println(prime);
                }
            } catch (FileNotFoundException e) {
                System.err.println("The file is not exist!");
            } catch (IOException e) {
                System.err.println("Something is wrong: " + e.getMessage());
            }

        } else {
            System.err.println("We need a file path as a parameter!");
        }
    }
}
