package cz.pivoda.primenumber;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class ReadPrimeNumber {

    public final static String COLUMN_NAME = "Data";

    private XSSFSheet sheet;
    private boolean isFirstRow = true;
    private int indexOfReadedColumn = -1;

    public void initFile(String filePath) throws FileNotFoundException, IOException {
        FileInputStream fis = new FileInputStream(new File(filePath));
        XSSFWorkbook wb = new XSSFWorkbook(fis);
        // get first sheet
        this.sheet = wb.getSheetAt(0);
        wb.close();
    }

    public List<Integer> getPrimeNumbers() {
        List<Integer> primes = new ArrayList<Integer>();

        for(Row row : sheet) {
            if (isFirstRow) {
                // find a right column
                indexOfReadedColumn = findColumn(row);
                isFirstRow = false;
            } else if(indexOfReadedColumn > -1) {
                Cell cell = row.getCell(indexOfReadedColumn);
                ReadValueFromExcelFile reader = ReadValueFactory.getReader(cell);
                if (reader != null) {
                    int prime = reader.getValue(cell);
                    if (isPrimeNumber(prime)) {
                        primes.add(prime);
                    }
                }
            }
        }

        return primes;
    }

    public boolean isPrimeNumber(int number) {
        if (number < 2) {
            return false;
        }
        for (int i = 2; i <= number/2; ++i) {
            // the prime number always has a nonzero remainder
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    public XSSFSheet getSheet() {
        return sheet;
    }

    /**
     * return index of column with prime numbers
     * @param firstRow
     * @return column_index
     */
    private static int findColumn(Row firstRow) {
        for (Cell cell : firstRow) {
            if (COLUMN_NAME.equals(cell.getStringCellValue())) {
                return cell.getColumnIndex();
            }
        }
        return -1;
    }
}
