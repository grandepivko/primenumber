package cz.pivoda.primenumber;

import org.apache.poi.ss.usermodel.Cell;

/**
 * @author Marek Pivoda (pivodmar)
 * 2020-06-05
 */
public class ReadNumberValue implements ReadValueFromExcelFile {

    @Override
    public int getValue(Cell cell) {
        if (isInteger(cell.getNumericCellValue())) {
            return (int) cell.getNumericCellValue();
        }
        return 0;
    }

    /**
     * Check if a number is an integer
     * @param number
     * @return boolean
     */
    private boolean isInteger(double number){
        return Math.ceil(number) == Math.floor(number);
    }
}
